/**
 * Created by sathukorala on 4/30/15.
 */

(function ($) {
    var clickcount = 0;
    var prevfid;
    function box_item_delete(itemtype,itemid, parentid) {
        if (itemtype === 'folder') {
            var callbackpath = "/box_connector/folder/delete";

        } else if (itemtype === 'file') {
            var callbackpath = "/box_connector/file/delete";
        }

        var SearchData = {itemid: itemid, parentid: parentid};
        Pace.track(function () {
            $.ajax({
                url: callbackpath,
                type: "POST",
                data: SearchData,
                success: function (data) {
                    $(".list-group").remove();
                    $(".flist").append(data);
                }
            })
        })
    }
    Drupal.CTools.Modal.modal_display = function(ajax, response, status) {
        $('#modal-title').html(response.title);
        $('#modal-content').html(response.output);
        $(document).trigger('CToolsAttachBehaviors', $('#modalContent'));
        Drupal.attachBehaviors();
    }

    $(document).on("CToolsAttachBehaviors", function() {
            var parentfolder = $('ul.breadcrumb').find('li.active').attr('id');
            var parentfldrattr = parentfolder.split("-");
            var parentid = parentfldrattr[1];
            console.log('ctools show');
            $('input[name*=box_connector_file_upload_upload_button][value=Upload][type=submit]').hide();
            $('input[name*=box_connector_file_upload_remove_button][value=Remove][type=submit]').hide();
            $('input[name="box_connector_parent_id"]').val(parentid);
    });

    Drupal.behaviors.box_connector = {
        attach: function(context, settings) {
            //Using the custom upload button to trigger the file fields upload button
            //click event after that show the remove button and hide this button
            $(document).on("click", '#box_connector_upload_button', function() {
                $('input[name*=box_connector_file_upload_upload_button][value=Upload][type=submit]').trigger('click');
                $(this).hide();
                $('#box_connector_remove_upload_button').show();
            });

            $(document).on("click", '#box_connector_remove_upload_button', function() {
                $('input[name*=box_connector_file_upload_remove_button][value=Remove][type=submit]').trigger('click');
                $(this).hide();
                $('#box_connector_upload_button').show();
            });

            $(document).ajaxComplete(function(event, xhr, settings) {
                if(settings.extraData != undefined && settings.extraData._triggering_element_value == "Submit") {
                    if(Drupal.settings.box_connector != undefined && Drupal.settings.box_connector.modal_submit == 1) {

                        Drupal.CTools.Modal.dismiss();
                        Drupal.settings.box_connector.modal_submit = 0;
                    }
                }

            });
        }
    }




    $(document).on("click", "li[id^='folder-']", function (event) {
        $(this).prop('disabled', true);
        var str = jQuery(this).attr("id");
        var foldername = $("#" + str + " div.item-name").text();
        var namesplit = str.split("-");
        var folderid = namesplit[1];
        var folder_data = {folderid: folderid};
            Pace.track(function () {
                $.ajax({
                    url: "/box_connector/folder/items",
                    type: "POST",
                    data: folder_data,
                    success: function (data) {
                        $(".list-group").remove();
                        $(".flist").append(data);
                        clickcount = clickcount + 1;

                        if (clickcount == 1) {
                            var hfolderid = 0;
                            $('.breadcrumb li').removeClass('active');
                            var lastelitext = $('.breadcrumb li').last().text();
                            $('.breadcrumb li').last().empty();
                            $(".breadcrumb li").last().append('<a id="b-' + hfolderid + '">' + lastelitext + '</a>');
                            $(".breadcrumb li").last().removeAttr("disabled");
                            $("ul.breadcrumb").append('<li class="active" id="d-' + folderid + '">' + foldername + '</li>');
                            $("#d-" + folderid).prop('disabled', true);
                            prevfid = folderid;
                        }

                        if (clickcount > 1) {
                            $('.breadcrumb li').removeClass('active');
                            var lastelitext = $('.breadcrumb li').last().text();
                            $('.breadcrumb li').last().empty();
                            $(".breadcrumb li").last().append('<a id="b-' + prevfid + '">' + lastelitext + '</a>');
                            $(".breadcrumb li").last().removeAttr("disabled");
                            prevfid = folderid;
                            $("ul.breadcrumb").append('<li class="active" id="d-' + folderid + '">' + foldername + '</li>');
                            $("#d-" + folderid).prop('disabled', true);

                        }
                    }
                })
            })
    });

    $(document).on("click", "li[id^='d-']", function () {
        $(this).prop('disabled', true);
        var str = jQuery(this).attr("id");
        var foldername = $("#" + str + " a").text();
        var namesplit = str.split("-");
        var folderid = namesplit[1];
        var SearchData = {folderid: folderid};
        Pace.track(function () {
            $.ajax({
                url: "/box_connector/folder/items",
                type: "POST",
                data: SearchData,
                success: function (data) {
                    $(".list-group").remove();
                    $(".flist").append(data);
                    $("#" + str + "").nextAll().remove();
                    $("#" + str + " a").remove();
                    $("#" + str + "").addClass("active");
                    $("#" + str + "").text(foldername);
                }
            })
        })

    });

    $(document).on('change', "select[id^='more-']", function (e) {
        var str = jQuery(this).attr("id");
        var parent = $(this).parents('div').attr('id');
        var parentattr = parent.split("-");
        var parentid = parentattr[0];
        var namesplit = str.split("-");
        var itemid = namesplit[1];

        var item = $(this).parents('li.list-edit').siblings('li.list-gp').attr('id');
        var itemattr = item.split("-");
        var itemtype = itemattr[0];

        if ($(e.target).val() == 'del') {
            if(itemtype == 'file') {
                r = confirm("Are you sure you want to delete the file?");
            } else {
                r = confirm("Are you sure you want to delete the folder  and all subfolders?");
            }

            if (r == true) {
                box_item_delete(itemtype, itemid, parentid);
            } else {
                $("#" + str).val('volvo');
            }

        } else if ($(e.target).val() == 'rname') {
            var parent = $(this).parents('div').attr('id');

            $("#" + parent).append('<div class="folder-rename ignore_click" style="display: block; position: absolute;  top: 4px; left: 38px;">' +
                '<input class="ignore_click item_value_input valid" style="width: 235px; height: 24px;" type="text" autocomplete="off" name="value">' +
                '<span class="action_cntr">' +
                '<button class="btn btn-primary ignore_click_handler edit_in_place_submit" type="submit">Save</button>' +
                '<button class="btn btn-primary edit_in_place_cancel" type="cancel">Cancel</button>' +
                '</span>' +
                '</div>')

            if (itemtype === 'folder') {
                var foldername = $("#" + parent + " div.item-name").text();
                $("#" + parent + " div input[type='text'].item_value_input").val(foldername);

            } else if (itemtype === 'file') {
                var filename = $("#" + parent + " div.item-name").text();
                //file name without extension
                var filenametxt = filename.substring(0, filename.lastIndexOf("."));
                $("#" + parent + " div input[type='text'].item_value_input").val(filenametxt);
            }


        }
    });

    $(document).on("click", ".edit_in_place_submit", function (event) {
        var folder = $(this).parents('div.folder-rename').siblings('li.list-gp').attr('id');
        var parentattr = folder.split("-");
        var itemtid = parentattr[1];
        var itemtype = parentattr[0];

        var itemname = $(this).parents('div.folder-rename').find('input').val();
        var parentfolder = $(this).parents('div.list-group-item').attr('id');
        var parentfldrattr = parentfolder.split("-");
        var parentid = parentfldrattr[0];



        if (itemtype === 'folder') {
            var SearchData = {itemtid: itemtid, itemname: itemname, parentid: parentid};
            Pace.track(function () {
                $.ajax({
                    url: "/box_connector/folder/rename",
                    type: "POST",
                    data: SearchData,
                    success: function (data) {
                        $(".list-group").remove();
                        $(".flist").append(data);
                    }
                })
            })
        } else if (itemtype === 'file') {
            var file_name = $(this).parents('div.folder-rename').siblings('li.list-gp').find('.item-name').html().trim();
            if(file_name.lastIndexOf(".") != -1) {
                var file_extension = file_name.substring(file_name.lastIndexOf(".") + 1, file_name.length);
                itemname = itemname + "." + file_extension
            }

            var SearchData = {itemtid: itemtid, itemname: itemname, parentid: parentid};
            Pace.track(function () {
                $.ajax({
                    url: "/box_connector/file/rename",
                    type: "POST",
                    data: SearchData,
                    success: function (data) {
                        $(".list-group").remove();
                        $(".flist").append(data);
                        //$("#" + str + "").nextAll().remove();
                        //$("#" + str + " a").remove();
                        //$("#" + str + "").addClass("active");
                    }
                })
            })
        }

    })
    $(document).on("click", ".edit_in_place_cancel", function (event) {
        $(this).parents('div.folder-rename').siblings('li.list-edit').find('select').val('volvo');
        $(this).parents('div.folder-rename').remove();
    })

    $(document).on('change', "select[id='new-select']", function (e) {

        if ($(e.target).val() == 'newfolder') {
            $(".list-new-div").html('<div class="new-folder-create ignore_click" style="display: block; position: absolute;  top: 4px; left: 38px;">' +
                '<label>Folder Name :</label><input class="ignore_click folder-name-input valid" style="width: 235px; height: 24px;" type="text" autocomplete="off" name="value">' +
                '<span class="action_cntr">' +
                '<button class="btn btn-primary new-folder-save" type="submit">Save</button>' +
                '<button class="btn btn-primary new-folder-save_cancel" type="cancel">Cancel</button>' +
                '</span>' +
                '</div>');
            $(this).val('new');
        }
    });

    $(document).on('change', "#upload-select", function (e) {
        if ($(e.target).val() == 'file-upload') {
            $(this).val('upload');
            $('#hidden-modal').trigger('click');
        }
    });

    $(document).on("click", ".new-folder-save_cancel", function (event) {
        $(this).parents('div.list-new-select').find('select').val('new');
        $(this).parents('div.new-folder-create').remove();
    })

    $(document).on("click", ".new-folder-save", function (event) {
        var new_folder_name = $(this).parents('div.new-folder-create').find('.folder-name-input').val();
        var list_group_id = $('div.list-group-item').attr('id');

        if (list_group_id === undefined || list_group_id === null) {
            var parentfolder = $('ul.breadcrumb').find('li.active').attr('id');
            var parentfldrattr = parentfolder.split("-");
            var parentid = parentfldrattr[1];
        } else {
            var parentfolder = $('div.list-group-item').attr('id');
            var parentfldrattr = parentfolder.split("-");
            var parentid = parentfldrattr[0];
        }

        console.log('parent:    ' + parentid);
        $("div.new-folder-create").hide();
        var create_data = {newfoldername: new_folder_name, parentid: parentid};
        Pace.track(function () {
            $.ajax({
                url: "/box_connector/folder/create",
                type: "POST",
                data: create_data,
                success: function (data) {
                    $(".list-group").remove();
                    $(".flist").append(data);
                }
            })
        })
    })


    $(document).mouseup(function (e) {
        var container1 = $("div.folder-rename");
        var container2 = $("div.new-folder-create");

        if (!container1.is(e.target) && container1.has(e.target).length === 0) {
            container1.hide();
            $('li.list-edit').find('select').val('volvo');
        }
        if (!container2.is(e.target) && container2.has(e.target).length === 0) {
            container2.hide();
        }
    });

    /* Use to View the document , - send request to get the document id and session view url via View API */

    $(document).on("click", "li[id^='file-']", function (event) {
        var str = jQuery(this).attr("id");
        var filename = $("#" + str + " div.item-name").text();
        var namesplit = str.split("-");
        var fileid = namesplit[1];
        var SearchData = {fileid: fileid};

        Pace.track(function () {
            $.ajax({
                url: "/box_connector/file/info",
                type: "POST",
                data: SearchData,
                success: function (data) {
                    data = eval("(" + data + ")");
                    console.log(data);
                    //window.location.replace(data);
                    window.open(data, '_blank');

                }
            })
        })
    });

})(jQuery);
