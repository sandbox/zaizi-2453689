<?php
/**
 * Created by PhpStorm.
 * User: sathukorala
 * Date: 6/23/15
 * Time: 11:47 AM
 */


/**
 * Callback function to get the box file download link from location header
 */
function box_connector_file_info() {
  $box_connector_user_id = arg(1);
  module_load_include('inc', 'box_connector', 'includes/box_connector.content.api');
  box_connector_box_caller($box_connector_user_id);
  $file_id = filter_xss($_POST['fileid']);
  \Box\BoxHTTPService::$viewAPIKey = variable_get('box_connector_view_API_key');
  //get the view file info
  $view_doc_info = box_connector_get_mapping_view_file_for_content_file($file_id);
  $document_id = $view_doc_info['id'];
  $session_details = box_connector_get_view_session_details_for_doc($document_id, $file_id);
  $document_view_url = $session_details['urls']['view'];
  echo json_encode($document_view_url);
  exit();
}

/**
 * A function to get write and set sessions per doc in cache
 * @param $document_id
 * @param $file_id
 * @param bool $reset
 */
function box_connector_get_view_session_details_for_doc($document_id, $file_id, $reset = FALSE) {
  //Get sessions for view doc
  if (!$reset && ($cache = cache_get('box_connector_view_sessions')) && !empty($cache->data)) {
    error_log('cache view session');
    $view_session_array = unserialize($cache->data);
    if(array_key_exists($file_id, $view_session_array) && !box_connector_check_if_session_has_expired($view_session_array[$file_id])){
      error_log('grab the view session from cache');
      $session_details = $view_session_array[$file_id];
    } else {
      $session_details = box_connector_get_session_for_view_doc($document_id);
      $view_session_array[$file_id] = $session_details;
      cache_set('box_connector_view_sessions', serialize($view_session_array));

    }
  } else {
    $view_session_array = array();
    $session_details = box_connector_get_session_for_view_doc($document_id);
    $view_session_array[$file_id] = $session_details;
    cache_set('box_connector_view_sessions', serialize($view_session_array));
  }

  return $session_details;
}

/**
 * A function to get a session for view document
 * @param $document_id
 * @return array|bool|float|int|null|string
 */
function box_connector_get_session_for_view_doc($document_id) {
  $session = new Box\View\Sessions\Sessions();
  do {
    $session_details = $session->createSession($document_id);
    $document_view_url = $session_details['urls']['view'];
  } while (is_null($document_view_url));
  return $session_details;
}

/**
 * Check if the Box View API session has expired
 */
function box_connector_check_if_session_has_expired($session_details) {
  $time = strtotime($session_details['expires_at']);
  $now = time();
  if ($now >= $time) {
    return true;
  } else {
    return false;
  }
}
/**
 * A function to get the file info by file ID
 * The funtion will also cache the file info against the file id
 * @param $file_id content API file id
 * @return array|null|string
 */
function box_connector_get_file_info_by_id($file_id, $reset = FALSE)
{
  // check if the info array is set or cached
  if (!$reset && ($cache = cache_get('box_connector_file_info')) && !empty($cache->data)) {
    error_log('cache file info');
    $info_array = unserialize($cache->data);
    //check if the current file is in the array
    if (array_key_exists($file_id, $info_array)) {
      error_log('cache file info found');
      $info = $info_array[$file_id];
    } else {
      //if the file is not in the array
      //add it and re-set the cache
      error_log('cache file info not found');
      $info = box_connector_get_file_info($file_id);
      $info_array[$file_id] = $info;
      cache_set('box_connector_file_info', serialize($info_array));
    }
  } else {
    //if the array is not set set it and
    //add it as cached data
    error_log('no cache file info');
    $info_array = array();
    $info = box_connector_get_file_info($file_id);
    $info_array[$file_id] = $info;
    cache_set('box_connector_file_info', serialize($info_array));
  }

  return $info;
}

/**
 * A function to get file info from the Content API
 * @param $file_id
 * @return array|null|string
 */
function box_connector_get_file_info($file_id)
{
  $box_file = new \Box\Content\Files\Files();
  $file_info_response = $box_file->getFileInfo($file_id);
  return $file_info_response->getInfo();
}

/**
 * A function to get the mapping view file given the content file id
 * @param $file_id
 * @param bool $reset
 * @return array|null|string|void
 */
function box_connector_get_mapping_view_file_for_content_file($file_id, $reset = FALSE)
{
  $content_doc_info = box_connector_get_file_info_by_id($file_id);
  $download_url = $content_doc_info['url'];

  if (!$reset && ($cache = cache_get('box_connector_view_files')) && !empty($cache->data)) {
    error_log('cache view file info');
    $view_file_array = unserialize($cache->data);
    if (array_key_exists($file_id, $view_file_array)) {
      error_log('cache file info found');
      $view_doc_info = $view_file_array[$file_id];
    } else {
      error_log('cache view file info not found');
      $view_doc_info = box_connector_upload_document_to_view($download_url);
      $view_file_array[$file_id] = $view_doc_info;
      cache_set('box_connector_view_files', serialize($view_file_array));
    }
  } else {
    error_log('no cache view file info');
    $view_file_array = array();
    $view_doc_info = box_connector_upload_document_to_view($download_url);
    $view_file_array[$file_id] = $view_doc_info;
    cache_set('box_connector_view_files', serialize($view_file_array));
  }
  return $view_doc_info;

}

/**
 * @param $download_url
 * A function to upload a document to the view API
 */
function box_connector_upload_document_to_view($download_url)
{
  $box_document = new \Box\View\Documents\Documents();
  $document_details = $box_document->URLUpload($download_url);
  return $document_details;
}


