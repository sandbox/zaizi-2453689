<?php
/**
 * @param bool $reset
 * @return \Box\Content\OAuth\OAuthService|mixed
 * A function to get the OAuthService instance
 */
function box_connector_initialize_oauth($reset = FALSE) {
  $box_connector_client_id = variable_get('box_connector_client_id');
  $box_connector_client_secret = variable_get('box_connector_client_secret');
  $box_connector_redirect_uri = variable_get('box_connector_redirect_uri');

  static $oauth;
  if (!isset($oauth) || $reset) {
    error_log('not static oauth');
    if (!$reset && ($cache = cache_get('box_connector_oauth', 'cache')) && !empty($cache->data)) {
      error_log('cahce oauth');
      $oauth = unserialize($cache->data);
    }
    else {
      error_log('no cache oauth');
      $oauth = new \Box\Content\Oauth\OAuthService($box_connector_client_id, $box_connector_client_secret, $box_connector_redirect_uri);

      cache_set('box_connector_oauth', serialize($oauth), 'cache', 60 * 60);
    }
  }
  return $oauth;
}

/**
 * @param $box_connector_user_id
 * @param bool $reset
 * @return bool|mixed
 * A function to check if a user has a token in the database
 */
function box_connector_does_user_token_exists($box_connector_user_id, $reset = FALSE) {
  static $token_exists;
  if (!isset($token_exists) || $reset) {
    error_log('not static ue');

    if (!$reset && ($cache = cache_get('box_connector_user_exists', 'cache')) && !empty($cache->data)) {
      error_log('cahce ue');
      $token_exists = unserialize($cache->data);
    }
    else {
      error_log('no cache ue');
      $user_count = box_connector_get_user_oauth_details($box_connector_user_id, TRUE);
      if ($user_count > 0) {
        $token_exists = TRUE;
        cache_set('box_connector_user_exists', serialize($token_exists), 'cache', 60 * 60);
      }
      else {
        $token_exists = FALSE;
      }

    }
  }
  return $token_exists;

}

/**
 * @param $box_connector_user_id
 * @param bool $reset
 * @return mixed
 * A function to get the particular users OAuth tokens
 */
function box_connector_get_user_oauth_tokens($box_connector_user_id, $reset = FALSE) {
  static $oauth_details;
  if (!isset($oauth_details) || $reset) {
    error_log('not static oauth');
    if (!$reset && ($cache = cache_get('box_connector_user_tokens', 'cache')) && !empty($cache->data)) {
      error_log('CACHE oauth');
      $oauth_details = unserialize($cache->data);
    }
    else {
      error_log('not CACHE oauth');
      $oauth_details = box_connector_get_user_oauth_details($box_connector_user_id);
      cache_set('box_connector_user_tokens', serialize($oauth_details), 'cache', 60 * 60);
    }

  }
  return $oauth_details;
}

/**
 * @param $uid
 */
function box_connector_box_caller($uid) {
  $user_token_exists = box_connector_does_user_token_exists($uid);
  $oauth = box_connector_initialize_oauth();
  if ($user_token_exists) {
    $oauth_details = box_connector_get_user_oauth_tokens($uid);
    $is_expired = $oauth->isExpired($oauth_details['expires_in'], $oauth_details['timestamp']);
    if ($is_expired) {
      \Box\BoxHTTPService::setRefreshToken($oauth_details['refresh_token']);
      $oauth_token = $oauth->getTokens();
      $box_connector_expires_in = $oauth_token['expires_in'];
      $box_connector_restricted_to = json_encode($oauth_token['restricted_to']);
      $box_connector_token_type = $oauth_token['token_type'];
      $box_connector_timestamp = time();
      box_connector_db_update($uid, $box_connector_expires_in, $box_connector_restricted_to, $box_connector_token_type, $box_connector_timestamp);
      //resetting the cache since it got updated
      $oauth_details = box_connector_get_user_oauth_tokens($uid, TRUE);
    }
    else {

      $oauth->setRequestTokens($oauth_details['access_token'], $oauth_details['refresh_token']);
    }
  }
  else {

    $oauth->getCode();
  }
}

/**
 * The callback function for BOX API
 */
function box_connector_get_token() {
  global $user;
  $box_connector_user_id = $user->uid;
  $code = $_GET['code'];
  $oauth = box_connector_initialize_oauth();
  $oauth_token = $oauth->getTokens($code);
  $box_connector_expires_in = $oauth_token['expires_in'];
  $box_connector_restricted_to = json_encode($oauth_token['restricted_to']);
  $box_connector_token_type = $oauth_token['token_type'];
  $box_connector_timestamp = time();
  $result = box_connector_db_insert($box_connector_user_id, $box_connector_expires_in, $box_connector_restricted_to, $box_connector_token_type, $box_connector_timestamp);
}


function box_connector_db_insert($box_connector_user_id, $box_connector_expires_in, $box_connector_restricted_to, $box_connector_token_type, $box_connector_timestamp) {
  $tid = db_insert('box_connector_access_token_log')
    ->fields(array(
      'uid' => $box_connector_user_id,
      'access_token' => \Box\BoxHTTPService::$accessToken,
      'expires_in' => $box_connector_expires_in,
      'restricted_to' => $box_connector_restricted_to,
      'refresh_token' => \Box\BoxHTTPService::$refreshToken,
      'token_type' => $box_connector_token_type,
      'timestamp' => $box_connector_timestamp,
    ))
    ->execute();

  drupal_goto('user/' . $box_connector_user_id . '/box');
}

function box_connector_db_update($box_connector_user_id, $box_connector_expires_in, $box_connector_restricted_to, $box_connector_token_type, $box_connector_timestamp) {
  $tid = db_update('box_connector_access_token_log')
    ->fields(array(
      'access_token' => \Box\BoxHTTPService::$accessToken,
      'expires_in' => $box_connector_expires_in,
      'restricted_to' => $box_connector_restricted_to,
      'refresh_token' => \Box\BoxHTTPService::$refreshToken,
      'token_type' => $box_connector_token_type,
      'timestamp' => $box_connector_timestamp,
    ))
    ->condition('uid', $box_connector_user_id, '=')
    ->execute();
}

function box_connector_get_user_oauth_details($uid, $get_count = FALSE) {
  $result = db_select('box_connector_access_token_log', 'c')
    ->fields('c')
    ->condition('c.uid', $uid, '=')
    ->execute();
  if ($get_count) {
    $result = $result->rowCount();
  }
  else {
    error_log('getting the user');
    $result = $result->fetchAssoc();
  }
  return $result;
}


function box_connector_folder_view($data) {
  $folder_list = box_connector_folder_list($data, 0, 1);
  drupal_add_css(drupal_get_path('module', 'box_connector') . '/templates/css/bootstrap.css', 'file');
  drupal_add_css(drupal_get_path('module', 'box_connector') . '/templates/css/boxstyles.css', 'file');
  drupal_add_css(drupal_get_path('module', 'box_connector') . '/templates/css/cornerindicator.css', 'file');


  drupal_add_js(drupal_get_path('module', 'box_connector') . '/js/folderdetails.js', 'file');
  drupal_add_js(drupal_get_path('module', 'box_connector') . '/js/pace.min.js', 'file');

  return theme('box_connector_view', array('page' => $folder_list));

}

function box_connector_folder_list($data, $parent_id, $box_connector_user_id = '') {
  $folder_list = "";
  box_connector_box_caller($box_connector_user_id);
  foreach ($data['entries'] as $item) {
    $icon_type = box_connector_item_icon_type($item);
    $item_type = $item['type'];
    if ($item_type == 'folder') {
      $elem_id = "folder-";
    }
    else {
      $elem_id = "file-";
    }
    $folder_list .= '<ul class="list-group">
                            <div class="list-group-item"  id="' . $parent_id . '-' . $item['id'] . '">
                                <li class="list-edit">
                                    <select id="more-' . $item['id'] . '">
                                      <option value="volvo">Select an Action</option>
                                      <option value="del">Delete</option>
                                      <option value="rname">Rename</option>
                                    </select>
                                </li>
                                <li class="list-gp" id="' . $elem_id . $item['id'] . '">
                                    <div class="img">
                                        <div class="thumb_small ' . $icon_type . '">
                                        </div>
                                    </div>
                                    <div class="item-name highlight">' . $item['name'] . '</div>
                                </li>
                            </div>
                        </ul>';

  }

  return $folder_list;
}

function box_connector_item_icon_type($item) {
  $icon_type = '';
  if ($item['type'] == 'folder') {
    $icon_type = 'sprite_32x32_folder';
  }
  elseif ($item['type'] == 'file') {
    $item_extension = pathinfo($item['name'], PATHINFO_EXTENSION);
    switch ($item_extension) {
      case 'gdoc':
        $icon_type = 'sprite_doc_32_32-gdoc';
        break;
      case 'pdf':
        $icon_type = 'sprite_doc_32_32-pdf';
        break;
      case 'jpg':
        $icon_type = 'sprite_doc_32_32-jpg';
        break;
      case 'jpeg':
        $icon_type = 'sprite_doc_32_32-jpeg';
        break;
      case 'png':
        $icon_type = 'sprite_doc_32_32-png';
        break;
    }
  }
  return $icon_type;
}

function box_connector_folder_items($variables) {
  global $user;
  $box_connector_user_id = $user->uid;
  box_connector_box_caller($box_connector_user_id);
  $folder_id = $_POST['folderid'];
  $box_folder = new \Box\Content\Folders\Folders();
  $folder_items = $box_folder->getFolderItems($folder_id);
  $folder_list = box_connector_folder_list($folder_items, $folder_id, $box_connector_user_id);
  echo $folder_list;

}

/**
 * @param $variables
 * @return string
 * A callback function to get all items of the root folder
 * for the particular user
 */
function box_connector_get_root_folder($variables) {
  ctools_include('modal');
  ctools_modal_add_js();
  drupal_add_js(array(
    'resize' => array(
      'modalSize' => array(
        'type' => 'fixed',
        'width' => 500,
        'height' => 250,
      ),
      //Animation mode
      'animation' => 'fadeIn',
    ),
  ), 'setting');

  global $user;
  $box_connector_user_id = $user->uid;
  box_connector_box_caller($box_connector_user_id);
  $box_folder = new \Box\Content\Folders\Folders();
  $folder_items = $box_folder->getFolderItems('0');
  return box_connector_folder_view($folder_items);
}

function box_connector_folder_delete($folder_id) {
  global $user;
  $box_connector_user_id = $user->uid;
  box_connector_box_caller($box_connector_user_id);
  $folder_id = $_POST['itemid'];
  $parent_id = $_POST['parentid'];
  $box_folder = new \Box\Content\Folders\Folders();
  $opts['recursive'] = 'true';
  $folder_responce = $box_folder->deleteFolder($folder_id, $opts);
  $folder_items = $box_folder->getFolderItems($parent_id);
  $folder_list = box_connector_folder_list($folder_items, $parent_id, $box_connector_user_id);
  echo $folder_list;
}

function box_connector_folder_rename($var) {
    global $user;
    $box_connector_user_id = $user->uid;
    box_connector_box_caller($box_connector_user_id);

  $folder_id = $_POST['itemtid'];
  $folder_name = $_POST['itemname'];
  $parent_id = $_POST['parentid'];

  $params = array("name" => $folder_name);
  $box_folder = new \Box\Content\Folders\Folders();
  $box_folder->updateFolder($folder_id, $params);
  $folder_items = $box_folder->getFolderItems($parent_id);
  $folder_list = box_connector_folder_list($folder_items, $parent_id, $box_connector_user_id);
  echo $folder_list;

}

function box_connector_folder_create($variables) {
  global $user;
  $box_connector_user_id = $user->uid;
  box_connector_box_caller($box_connector_user_id);
  $new_folder_name = $_POST['newfoldername'];
  $parent_id = $_POST['parentid'];
  $box_folder = new \Box\Content\Folders\Folders();
  $folder_response = $box_folder->createFolder($new_folder_name, $parent_id);
  $folder_items = $box_folder->getFolderItems($parent_id);
  $folder_list = box_connector_folder_list($folder_items, $parent_id, $box_connector_user_id);
  echo $folder_list;
}

function box_connector_file_rename($variables) {
  global $user;
  $box_connector_user_id = $user->uid;
  box_connector_box_caller($box_connector_user_id);
  $itemt_id = $_POST['itemtid'];
  $item_name = $_POST['itemname'];
  $parent_id = $_POST['parentid'];

  $params = array("name" => $item_name);
  $box_file = new \Box\Content\Files\Files();
  $box_folder = new \Box\Content\Folders\Folders();

  $file_rename_responce = $box_file->updateFile($itemt_id, $params);
  $folder_items = $box_folder->getFolderItems($parent_id);
  $folder_list = box_connector_folder_list($folder_items, $parent_id, $box_connector_user_id);
  echo $folder_list;
}

function box_connector_file_delete($variable) {
  global $user;
  $box_connector_user_id = $user->uid;
  box_connector_box_caller($box_connector_user_id);
  $item_id = $_POST['itemid'];
  $parent_id = $_POST['parentid'];

  $box_file = new \Box\Content\Files\Files();
  $box_folder = new \Box\Content\Folders\Folders();

  $file_rename_responce = $box_file->deleteFile($item_id);
  $folder_items = $box_folder->getFolderItems($parent_id);
  $folder_list = box_connector_folder_list($folder_items, $parent_id, $box_connector_user_id);
  echo $folder_list;

}



